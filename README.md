# GLOFF - Gcp Lazy OFF

## USAGE

`gloff` currently takes no parameters. It turns off all instances in all zones within a project

## TODO

### Create issues for below features
  - add a "-q / --quiet" flag to not return anything
  - add a "-v / --verbose" flag to return everything and show only instances that were stopped
  - add a "--on" turn on all insances
  - add a "-z / --zone" parameter to turn on, off or list instances only within a single zone
  - add a "-i / --instance" option that takes instance name as an argument and runs action on this instance only
  - extract project_id from the JSON keys file below
